var sinon = require('sinon');
var chai = require('chai');
var expect = chai.expect;

var mongoose = require('mongoose');
require('sinon-mongoose');

var User = require('../model/user/user-model');
var Favorite = require('../model/favorite/favorite-model');

describe('Insert new User', function() {
    it('should create new user', function(done) {
        var UserMok = sinon.mock(new User({username: "challenge", pwd: "root123"}));
        var user = UserMok.object;
        var expectedResult = { status: true };
        UserMok.expects('save').yields(null, expectedResult);
        user.save(function (err, result) {
            UserMok.verify();
            UserMok.restore();
            expect(result.status).to.br.true;
            done();
        });
    });

    it('should return error, if user not saved', function(done) {
        var UserMok = sinon.mock(new User({username: "challenge", pwd: "root123"}));
        var user = UserMok.object;
        var expectedResult = { status: false };
        UserMok.expects('save').yields(null, expectedResult);
        user.save(function (err, result) {
            UserMok.verify();
            UserMok.restore();
            expect(result.status).to.br.true;
            done();
        });
    });

});

describe('Get a user', function() {
    it('should get a user base in his username', function(done) {
        var UserMok = sinon.mock();
        var expectedResult = { status: true, username: "challenge", pwd:"root123" };
        UserMok.expects('find').yields(null, expectedResult);
        user.find(function (err, result) {
            UserMok.verify();
            UserMok.restore();
            expect(result.status).to.br.true;
            done();
        });
    });

    it('should return error', function(done) {
        var UserMok = sinon.mock();
        var expectedResult = { status: false, error: "User not found"  };
        UserMok.expects('find').yields(null, expectedResult);
        user.find(function (err, result) {
            UserMok.verify();
            UserMok.restore();
            expect(result.status).to.not.be.true;
            done();
        });
    });
});

describe('Insert a favorite', function() {
    it('should insert a new favorite location', function(done) {
        
        var FavMok = sinon.mock(new Favorite({host_name: "http://www.google.com.br/", host_ip: "0.0.0.0", user_id: "5a2e5a48dd0db33ac8b5be4d"}));
        var favorite = FavMok.object;
        var expectedResult = { status: true };
        FavMok.expects('save').yields(null, expectedResult);
        favorite.save(function (err, result) {
            UserMok.verify();
            UserMok.restore();
            expect(result.status).to.br.true;
            done();
        });
    });
})

describe('get all favorites, based on user_id', function() {
    it('should insert a new favorite location', function(done) {
        
        var FavMok = sinon.mock(new Favorite());
        var favorite = FavMok.object;
        var expectedResult = { status: true , user_id: "5a2e5a48dd0db33ac8b5be4d"};
        FavMok.expects('find').yields(null, expectedResult);
        favorite.find(function (err, result) {
            UserMok.verify();
            UserMok.restore();
            expect(result.status).to.br.true;
            done();
        });
    });
});

describe('delete favorite, based on favorite id', function() {
    it('remove favorite by id', function(done) {
        
        var FavMok = sinon.mock(new Favorite());
        var expectedResult = { status: true };
        FavMok.expects('remove').withArgs({_id: mongoose.Types.ObjectId("12a2dfbdbfd12345fd")}).yields(expectedResult, null);
        favorite.remove({_id: mongoose.Types.ObjectId("12a2dfbdbfd12345fd")}, function (err, result) {
            UserMok.verify();
            UserMok.restore();
            expect(result.status).to.br.true;
            done();
        });
    });
});
