const { Router } = require('express');
const path = require('path');

const router = new Router();

router.get('/', function(req, res, next) {
    res.sendFile(path.join(__dirname + '/index.html'));
});

module.exports = router;