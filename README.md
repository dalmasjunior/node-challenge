# Avenue Code NodeJS Challenge #

### How to Cake ###

#### Ingredients ####

* 1 - MongoDB as database;
* 2 - NodeJs installed ( preference for the latest version )
* 3 - Gulp to mix and assemble our entire code

#### Hands on Work ####

* First clone this repository with the command: git clone https://bitbucket.org/dalmasjunior/node-challenge.git
* Run the next commands to access the project and install all their dependencies:

* cd node-challenge && npm install

* Next we need open and edit the config.js file to fill with your MongoDB url;
* If you need help to install and configure your MongoDB, please take a look in the documentation at https://goo.gl/3NkfMk 
* I recommend you to use a container of MongoDB with Docker (https://goo.gl/52SL9m);
* After all execute the command gulp start
* Done, your app will beign executed on the port that you have configured
* You index.html will be open in your browser automagically.

### Selected Spices ###
* As I mentioned before, as database I'm using MongoDB. Because the experience and good response I've had in all the projects that I use.
* Mongoose was selected to manage connection, Schemas, Models and transactions with the database.
* For Unit tests  I've tried use Mocha with Sinon and Chai. I'm still fixing bugs in the tests files (O.O)'
* Express are used here because their documentation are so completed and his community make all this more easy to use. 
* Gulp was a new experience, and I like it.

### What I Have to Finish ###
* Unit tests.
* I'm still reading their documentations and trying to apply in this project.

### This is not the End ###

My name is Paulo Roberto and I'm glad to participate in this test.
If you need talk with me send me a email (dalmasjunior@gmail.com) or a message in the LinkedIn (https://www.linkedin.com/in/paulodalmas/)

"So long, and thanks for all the fish!"