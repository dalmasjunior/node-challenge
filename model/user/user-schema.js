const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const Schema = mongoose.Schema;

const userSchema = new Schema({
    username: {type: String, required: true},
    pwd: {type: String, required: true},
    lastLogin: {type: mongoose.Schema.Types.Date}
});

userSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Challenge-User', userSchema, 'challenge-user');