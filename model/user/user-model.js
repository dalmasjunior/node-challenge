const Model = require('../../lib/facade');
const mongoose = require('mongoose');
const userSchema = require('./user-schema');

class userModel extends Model {

    insert(user, callback, callbackErr) {
        const schema = new userSchema(user);

        schema.save(user, function (err, userReturn) {
            if (userReturn) {
                callback(userReturn);
            } else {
                callbackErr(err);
            }
        });
    };

    find(user) {
        return userSchema
                .find({
                    'username': user
                })
                .exec();
    }

    update(user) {
        return userSchema
                .findOneAndUpdate({
                    '_id': mongoose.Schema.Types.ObjectId(user)
                }, {
                    $set: {"lastLogin": new Date().toISOString()}
                })
                .exec();
    }

}

module.exports = new userModel(userSchema);