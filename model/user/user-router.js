const controller = require('../../controller/user-controller');
const { Router } = require('express');

const router = new Router();

router.route('/')
    .post((...args) => controller.create(...args));

router.route('/login')
    .post((...args) => controller.login(...args))

module.exports = router;