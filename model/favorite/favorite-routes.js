const controller = require('../../controller/favorite-controller');
const { Router } = require('express');

const router = new Router();

router.route('/')
    .post((...args) => controller.insert(...args));

router.route('/:id')
    .get((...args) => controller.list(...args))
    .delete((...args) => controller.remove(...args));

module.exports = router;