const Model = require('../../lib/facade');
const mongoose = require('mongoose');
const favoriteSchema = require('./favorite-schema');

class favoriteModel extends Model {

    insert(favorite, callback, callbackErr) {
        const schema = new favoriteSchema(favorite);

        schema.save(favorite, function (err, favoriteReturn) {
            if (favoriteReturn) {
                callback(favoriteReturn);
            } else {
                callbackErr(err);
            }
        })
    }

    list(user_id) {
        return favoriteSchema
                .find({
                    'user_id': user_id
                })
                .exec();
    }

    remove(favorite_id) {
        return favoriteSchema
                .find({
                    "_id": mongoose.Types.ObjectId(favorite_id)
                })
                .remove()
                .exec();
    }

};

module.exports = new favoriteModel(favoriteSchema);