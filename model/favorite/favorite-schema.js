const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const Schema = mongoose.Schema;

const favoriteSchema = new Schema({
    host_name: {type: String},
    host_ip: {type: String},
    user_id: {type: String}
});

favoriteSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Challenge-Favorite', favoriteSchema, 'challenge-favorite');