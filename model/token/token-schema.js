const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const tokenSchema = new Schema({
     name: {type: String},
     token_admin: {type: String}
});

module.exports = mongoose.model('Challenge-Token', tokenSchema, 'challenge-token');