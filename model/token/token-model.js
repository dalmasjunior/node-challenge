const Model = require('../../lib/facade');
const tokenSchema = require('./token-schema');

class tokenModel extends Model {

    permit(token, callback) {
        tokenSchema.findOne({token_admin: token})
                .exec(function (err, tokenReturn) {
                    if (err) {
                        console.log(JSON.stringify(err));
                        callback(false);
                    }
                    if (tokenReturn) {
                        console.log('token found.');
                        callback(true);
                    } else {
                        console.log('token not found');
                        callback(false);
                    }
                });
    }
};

module.exports = new tokenModel(tokenSchema);