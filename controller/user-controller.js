const Controller = require('../lib/controller');
const userModel = require('../model/user/user-model');
const tokenModel = require('../model/token/token-model');
const config = require('../config');

class UserController extends Controller {

    create(req, res) {

        var callbackSuccess = function (authenticated) {
            if (!authenticated) {
                res.status(401).end();
            }

            var user = req.body.user;

            userModel.find(user.username)
                .then(userReturn => {
                    if (userReturn[0]) {
                        res.status(400).json("This username already has a owner");
                    } else {
                        try {
                            userModel.insert(user,
                                        function (newUser) {
                                            res.status(201).json(newUser);
                                        }, function (err) {
                                            res.status(400).json(err);
                                        });
                        } catch (err) {
                            res.status(400).json('Errors saving user');
                        }
                    }
                })
                .catch(err => {
                    res.status(400).json(err);
                });
        };

        var callbackErr = function () {
            res.status(401).end();
        };

        this.authenticate(req, callbackSuccess, callbackErr);
    }

    login(req, res) {

        var callbackSuccess= function (authenticated) {

            if(!authenticated) {
                res.status(401).end();
            }

            var user = req.body.data;

            userModel.find(user.username)
                .then(userReturn => {
                    if (userReturn[0]) {
                        var match = user.pwd == userReturn[0].pwd ? true : false;

                        if (match) {
                            userModel.update(userReturn[0]._id);
                            res.status(201).json(userReturn[0]);
                        } else {
                            res.status(400).json('Password mismatch');
                        }
                    } else {
                        res.status(400).end('Username not found');
                    }
                })
                .catch(err => {
                    res.status(400).json(err);
                }) 
        };

        var callbackErr = function () {
            res.status(401).end();
        };

        this.authenticate(req, callbackSuccess, callbackErr);
    }

    authenticate(req, callback, callbackErr) {
        var authorization = req.headers['authorization'];

        if (authorization) {
            tokenModel.permit(authorization, function (data) {
                callback(data);
            });
        } else {
            callbackErr();
        }
    };

};

module.exports = new UserController(userModel);