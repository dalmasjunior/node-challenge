const Controller = require('../lib/controller');
const favoriteModel = require('../model/favorite/favorite-model');
const tokenModel = require('../model/token/token-model');
const config = require('../config');

class FavoriteController extends Controller {

    insert(req, res) {
        
        var callbackSuccess = function(authenticated) {
            if (!authenticated) {
                res.status(401).end();
            }

            var favorite = req.body;

            try {
                favoriteModel.insert(favorite, function (newFavorite) {
                    res.status(201).json(newFavorite);
                }, function (err) {
                    res.status(400).json(err);
                });
            } catch (err) {
                res.status(400).json(err);
            }           
        };

        var callbackErr = function () {
            res.status(401).end();
        };

        this.authenticate(req, callbackSuccess, callbackErr);
    };

    list(req, res) {
        var callbackSuccess = function (authenticated) {
            if (!authenticated) {
                res.status(401).end();
            }

            var user_id = req.params.id;

            favoriteModel.list(user_id)
                .then(favoriteList => {
                    if (favoriteList[0]) {
                        res.status(201).json(favoriteList);
                    } else {
                        res.status(400).json('No favorites found');
                    }
                })
                .catch(err => {
                    res.status(400).json(err);
                });
        };

        var callbackErr = function () {
            res.status(401).end();
        };

        this.authenticate(req, callbackSuccess, callbackErr);
    }

    remove(req, res) {

        var callbackSuccess = function (authenticated) {
            if (!authenticated) {
                res.status(401).end();
            }

            var favorite_id = req.params.id;

            favoriteModel.remove(favorite_id)
                .then(err => {
                    if (err.result.ok != 1) {
                        res.status(400).json(err);
                    } else {
                        res.status(201).json('Favorite removed');
                    }
                })
                .catch(err => {
                    res.status(400).json(err);
                });
        }

        var callbackErr = function () {
            res.status(401).end();
        };

        this.authenticate(req, callbackSuccess, callbackErr);
    }

    authenticate(req, callback, callbackErr) {
        var authorization = req.headers['authorization'];

        if (authorization) {
            tokenModel.permit(authorization, function (data) {
                callback(data);
            });
        } else {
            callbackErr();
        }
    };
};

module.exports = new FavoriteController(favoriteModel);