const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const helmet = require('helmet');
const bluebird = require('bluebird');
const morgan = require('morgan');

const config = require('./config');
const routes = require('./routes');

const app = express();

process.on('uncaughtException', function (err) {
    console.log(err);
});

mongoose.Promise = bluebird;
mongoose.connect(config.mongo.url);

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header("Access-Control-Allow-Headers", "authorization,content-type,cache-control");
    next();
});

app.use(express.static(__dirname + '/public'));
app.use(helmet());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(morgan('tiny'));
app.use('/', routes);

app.listen(config.server.port, () => {
    console.log(`Working on port ${config.server.port}`);
});

module.exports = app;