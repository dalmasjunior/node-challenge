function updateFavoritesList(user) {
    if (user.logged) {
        if(user.favorites) {
            window.favoritesTemplate = Handlebars.compile($('#favoritesTemplate').html());
            $("#favorites").html(window.favoritesTemplate(user.favorites));
            $("#favoriteList").show();
            $('.btn-danger').click(removeFavorite);
        } else {
            $("#favoriteList").hide();
        }
    }
};

function insertLocalKeyValue(key, value) {
    
    localStorage.setItem(key, JSON.stringify(value));
}

function getLocalKeyValue(key) {
    return JSON.parse(localStorage.getItem(key));
}

function updateFavoriteBtnTemplate(search) {
    var user = getLocalKeyValue('user');
    if (user.logged && search) {
        $('#favButton').show();    
        $('#favButton').on('click', function() {
            var data = JSON.stringify({
                host_name: $("#txtWebsite").val(),
                host_ip: $("#query").text(),
                user_id: user._id
            });
            $.ajax({
                url: '/favorite',
                type: 'POST',
                data: data,
                beforeSend: function(xhr){
                    xhr.setRequestHeader('Content-Type', 'application/json');
                    xhr.setRequestHeader('Authorization', 'fdf2c59819ec526ed47ca9d4e5a970aa');
                },
                success: function(data) {
                    saveLocalFavorites(user, [data])
                },
                error: function(err) {
                    console.log(err)
                }
            });
        })
    } else {
        $('#favButton').hide();
    }
}

function saveLocalFavorites(user, favorites) {
    if (!user.favorites) {
        user.favorites = [];
    } 
    for (var i = 0; i< favorites.length; i++){
        user.favorites.push(favorites[i]);   
    }
    
    insertLocalKeyValue('user', user);
    updateFavoritesList(user);
    
}

function removeFavorite(event) {
    var favId = event.currentTarget.dataset.id;

    $.ajax({
        url: '/favorite/' + favId,
        type: 'DELETE',
        beforeSend: function(xhr){
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.setRequestHeader('Authorization', 'fdf2c59819ec526ed47ca9d4e5a970aa');
        },
        success: function() {

            var user = getLocalKeyValue('user');
            
            user.favorites = $.grep(user.favorites, function(e) {
                return e._id != favId;
            });

            insertLocalKeyValue('user', user);
            updateFavoritesList(user);
        }
    });
}

(function() {
    
        'use strict';

    function getFavoritesList(user) {
        $.ajax({
            url: '/favorite/' + user._id,
            type: 'GET',
            beforeSend: function(xhr){
                xhr.setRequestHeader('Content-Type', 'application/json');
                xhr.setRequestHeader('Authorization', 'fdf2c59819ec526ed47ca9d4e5a970aa');
            },
            success: function(data) {
                console.log(data);
                saveLocalFavorites(user, data);
            },
            error: function(err) {
                console.log(err)
            }
        });
    }

    function updateFavoritesList(user) {
        if (user.logged) {
            if(user.favorites) {
                window.favoritesTemplate = Handlebars.compile($('#favoritesTemplate').html());
                $("#favorites").html(window.favoritesTemplate(user.favorites));
                $("#favoriteList").show();
            } else {
                $("#favoriteList").hide();
            }
        }
    }

    function getFormValuesJSON(form) {
        var data = {};
        
        form.find(":input").each(function() {
            data[this.name] = $(this).val();
        })

        return {data};
    }

    function updateLoginTemplate() {
        var user = getLocalKeyValue('user');

        if (user.logged) {
            $("#logged").show();
            $("#username").text(user.username);
            $("#logoff").hide();
        } else {
            $("#logoff").show();
            $("#logged").hide();
        }
    }

    function signUp(e) {
        e.preventDefault();
        var data = JSON.stringify(getFormValuesJSON($('#logInSignUpForm')));

        $.ajax({
            url: '/user',
            type: 'POST',
            data: data,
            beforeSend: function(xhr){
                xhr.setRequestHeader('Content-Type', 'application/json');
                xhr.setRequestHeader('Authorization', 'fdf2c59819ec526ed47ca9d4e5a970aa');
            },
            success: function(data) {
                console.log(data);

            },
            error: function(err) {
                console.log(err)
            }
        });
    }

    function logIn(e) {
        e.preventDefault();
        var data = JSON.stringify(getFormValuesJSON($('#logInSignUpForm')));
        $.ajax({
            url: '/user/login',
            type: 'POST',
            data: data,
            beforeSend: function(xhr){
                xhr.setRequestHeader('Content-Type', 'application/json');
                xhr.setRequestHeader('Authorization', 'fdf2c59819ec526ed47ca9d4e5a970aa');
            },
            success: function(data) {
                data.logged = true;
                insertLocalKeyValue('user', data);
                $("#logInSignUpModal").modal('hide');
                updateLoginTemplate();
                getFavoritesList(data);
            }
        });
    }
    
    function start() {
        $("#logoff").hide();
        $("#logged").hide();
        $('#favButton').hide();
        $("#favoriteList").hide();
        insertLocalKeyValue('user', {});
        updateLoginTemplate();

        $('#logInSignUpModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var action = button.data('action');
    
            var modal = $(this);
    
            modal.find('.modal-title').text(action == "register" ? "Sign Up" : "Log In");
            modal.find('#btnLogInSignIn').text(action == "register" ? "Sign" : "Enter");
            $(modal.find('#btnLogInSignIn')).on('click', action == "register" ? signUp : logIn);
        });
    }

    $(document).ready(start);
})();