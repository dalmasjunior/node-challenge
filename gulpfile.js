const gulp = require('gulp');
const nodemon = require('gulp-nodemon');
const open = require('gulp-open');
const config = require('./config');

const static = './static';

gulp.task('start',['op'], function() {
    nodemon({
        script: 'index.js',
        watch: ["index.html", "{,*/}*.js", "{,*/}*.css"],
        ext: 'js css'
    }).on('restart', () => {
        gulp.src('index.js');
    })
});

gulp.task('op', function() {
    var options = {
        uri: 'http://localhost:' + config.server.port,
    };
    gulp.src(__filename)
        .pipe(open(options));
})

