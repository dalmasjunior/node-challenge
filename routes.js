const { Router } = require('express');

const router = new Router();

const views = require('./views/views-routes');
const user = require('./model/user/user-router');
const favorite = require('./model/favorite/favorite-routes.js');


router.use('/user', user);
router.use('/', views);
router.use('/favorite', favorite);

module.exports = router;