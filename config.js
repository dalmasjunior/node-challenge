const config = {
    mongo: {
        url: process.env.MONGO_DB_URI || 'mongodb://pidap:P0l967560@processamento14.publicacoesonline.com.br:27017/pol-pidap'
    },
    server: {
        port: 9002
    }
}

module.exports = config;